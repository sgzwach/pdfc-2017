#!/usr/bin/python

import zlib
import hashlib
import sys

# get file from user
if len(sys.argv) < 2:
	print "ERROR: I need hash plz"

# determine path of objects
sha1 = sys.argv[1]
path = '.git/objects/' + sha1[:2] + '/' + sha1[2:]

# retrieve broken obj in question
cf = open(sha1, 'r')
db = cf.read()
cf.close()

# put blob size at the front
bd = "blob %d\x00" % (len(db))
bm = len(bd)
db = bd + db

# for every byte in the file do some magic
for i in xrange(bm, len(db)):
		
	# try all characters
	for c in xrange(256):
		guess = db[:i-1] + chr(c) + db[i:]

		# check hash
		hs = hashlib.sha1(guess).hexdigest()
		if hs == sha1:
			print "That'll do it"
			# rewrite commit
			cf = open(path, 'w')
			cf.write(zlib.compress(guess))
			cf.close()
			
			# write changes locally so we can diff
			cf = open(sha1 + "-fixed", 'w')
			cf.write(guess[bm:])
			cf.close()
			sys.exit(0)

# if we made it this far we're not winning
print "No matches :("
			
