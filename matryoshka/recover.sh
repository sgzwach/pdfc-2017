#!/bin/bash

myfile=$1

echo "DECompressing-ish $myfile..."

mytest=""

while [ "$mytest" != "flag" ]
do
	res=$(file $myfile | awk '{ print $2 }')
	echo "File is $res"
	if [ $res = "ASCII" ] || [ $res = "Minix" ]; then # <-- Minix is a false hit by the file command
		br=$(base64 -d $myfile > tmp)
		if [ $? -eq 1 ]; then
			cat $myfile
			break
		fi
	elif [ $res = "Zip" ]; then
		mv $myfile tmp.zip
		unzip -P matryoshka -p tmp.zip > tmp
	elif [ $res = "gzip" ]; then
		mv $myfile tmp.tgz
		tar xfO tmp.tgz > tmp
	elif [ $res = "7-zip" ]; then
		mv $myfile tmp.7z
		7z e -so tmp.7z > tmp
	elif [ $res = "bzip2" ]; then
		mv $myfile tmp.bz2
		tar xfO tmp.bz2 > tmp
	fi
	mv tmp unknown
	myfile="unknown"
done

# cleanup a bit
rm -f unknown tmp* -f > /dev/null