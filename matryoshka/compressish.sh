#!/bin/bash

myfile=$1

echo "Compressing-ish $myfile..."

for i in {1..354}
	do
		if [ $i -lt 100 ]; then
			r=$((RANDOM % 5))
		else
			r=$((RANDOM % 4))
		fi
		if [ $r -eq 0 ]; then
			tar cfz tmp.tgz $myfile
			hash=$(md5sum tmp.tgz | awk '{ print $1 }')
			mv tmp.tgz $hash
			rm $myfile
			myfile=$hash
		elif [ $r -eq 1 ]; then
			base64 $myfile > tmp.txt
			hash=$(md5sum tmp.txt | awk '{ print $1 }')
			mv tmp.txt $hash
			rm $myfile
			myfile=$hash
		elif [ $r -eq 2 ]; then
			zip --password matryoshka tmp.zip $myfile
			hash=$(md5sum tmp.zip | awk '{ print $1 }')
			mv tmp.zip $hash
			rm $myfile
			myfile=$hash
		elif [ $r -eq 3 ]; then
			7z a tmp.7z $myfile
			hash=$(md5sum tmp.7z | awk '{ print $1 }')
			mv tmp.7z $hash
			rm $myfile
			myfile=$hash
		elif [ $r -eq 4 ]; then
			tar cfj tmp.tbz $myfile
			hash=$(md5sum tmp.tbz | awk '{ print $1 }')
			mv tmp.tbz $hash
			rm $myfile
			myfile=$hash
		fi
		echo "Iteration $i done - $myfile"
	done